#ifndef WINDOWCONTENT_H
#define WINDOWCONTENT_H

#include <QWidget>

namespace Ui {
class WindowContent;
}

class WindowContent : public QWidget
{
    Q_OBJECT
public:
    WindowContent(QWidget* = 0);

private:
    bool window_move = false;
    QPoint mouse_pos;
    QString password;
    QList<QStringList> database;
    Ui::WindowContent *ui;
    bool eventFilter(QObject* target, QEvent* event);
    void mainWidgetInit();
    void cryptography();
    void decryption();
    void serviceEdit();

private slots:
    void screenMode(bool);
    void checkLang();
    void showPassword(bool);
    void enterPassword();
    void blockMenu();
    void fillTable();
};

#endif // WINDOWCONTENT_H
