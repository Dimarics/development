QT+= core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

SOURCES += \
    cryptography.cpp \
    datawidget.cpp \
    decryption.cpp \
    main.cpp \
    serviceeditor.cpp \
    table.cpp \
    window.cpp \
    windowcontent.cpp

HEADERS += \
    serviceeditor.h \
    table.h \
    window.h \
    windowcontent.h

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

FORMS += \
    serviceeditor.ui \
    window.ui \
    windowcontent.ui

RESOURCES += \
    resources.qrc
