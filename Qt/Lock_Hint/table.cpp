#include "table.h"

Table::Table(QWidget *parent) : QTableWidget(parent)
{
    setMouseTracking(true);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    verticalHeader()->setVisible(false);

    horizontalHeader()->setFixedHeight(40);
    horizontalHeader()->hideSection(3);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    horizontalHeader()->setSectionsClickable(false);
    verticalScrollBar()->setTracking(true);
    setItemDelegate(new TableDelegate);

    option = new QMenu(this);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested, this, &Table::contextMenu);
    QAction *copy = new QAction("Копировать", this);
    option->addAction(copy);
    connect(copy, &QAction::triggered, this, &Table::copyText);
    edit = new QAction("Изменить", this);
    option->addAction(edit);
    del = new QAction("Удалить", this);
    option->addAction(del);
}

void Table::setTextColor(const QColor &color) {
    static_cast<TableDelegate*>(itemDelegate())->textColor = color;
}

void Table::focusOutEvent(QFocusEvent*) {
    clearSelection();
}

/*void Table::wheelEvent(QWheelEvent* event)
{
    int step;
    event->angleDelta().y() > 0 ? step = -1 : step = 1;
    verticalScrollBar()->setValue(verticalScrollBar()->value() + step);
    verticalScrollBar()->setMaximum(rowCount() - 10);
}*/

void Table::contextMenu(QPoint pos) {
    option->popup(viewport()->mapToGlobal(pos));
}

void Table::copyText()
{
    if (item(currentRow(), currentColumn()) != 0)
    {
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(item(currentRow(), currentColumn())->text());
    }
}

void Table::resizeEvent(QResizeEvent*) {
    horizontalHeader()->setDefaultSectionSize(width() / 3 - 5);
}

void TableDelegate::paint(QPainter* painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem itemOption(option);
    if (itemOption.state & QStyle::State_HasFocus)
    {
        itemOption.state = itemOption.state ^ QStyle::State_HasFocus;
        painter->fillRect(option.rect, QColor(255, 255, 255, 40));
    }
    itemOption.palette.setColor(QPalette::HighlightedText, textColor);
    /*if (text_color == 0)
        itemOption.palette.setColor(QPalette::HighlightedText, Qt::white);
    else if (text_color == 1)
        itemOption.palette.setColor(QPalette::HighlightedText, Qt::yellow);
    else if (text_color == 2)
        itemOption.palette.setColor(QPalette::HighlightedText, QColor(0, 255, 0));*/
    QStyledItemDelegate::paint(painter, itemOption, index);
}
