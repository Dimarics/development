#include "schemegraphics.h"
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>

SchemeGraphics::SchemeGraphics() : QGraphicsScene()
{
    mouse_pressed = false;
    insertedItem = nullptr;

    setSceneRect(0, 0, sceneSize, sceneSize);
    setItemIndexMethod(QGraphicsScene::BspTreeIndex);
}

void SchemeGraphics::clearScene()
{
    generators.clear();
    knots.clear();
    clear();
}

void SchemeGraphics::removeSelectedItems()
{
    for (QGraphicsItem* selectedItem : selectedItems())
    {
        SchemeItem *item = static_cast<SchemeItem*>(selectedItem);
        switch (item->type())
        {
        case SchemeItem::TypeGen:
            generators.removeOne(item);
            break;
        case SchemeItem::TypeKnot:
            knots.removeOne(item);
            break;
        default:
            break;
        }

        removeItem(item);
        delete item;
    }
    for (int i = 0; i < generators.size(); ++i)
        generators[i]->setId(i + 1);
    for (int i = 0; i < knots.size(); ++i)
        knots[i]->setId(i + 1);
}

void SchemeGraphics::setMode(Mode set_mode)
{
    mode = set_mode;
    if (insertedItem != nullptr)
    {
        removeItem(insertedItem);
        delete insertedItem;
        insertedItem = nullptr;
    }

    switch (mode)
    {
    case None:
        static_cast<QGraphicsView*>(views().at(0))->setDragMode(QGraphicsView::NoDrag);
        for (QGraphicsItem* item : items())
            if (item->type() > QGraphicsItem::UserType)
                item->setFlags(QGraphicsItem::ItemIsSelectable);
        break;
    case Grab:
        clearSelection();
        static_cast<QGraphicsView*>(views().at(0))->setDragMode(QGraphicsView::ScrollHandDrag);
        for (QGraphicsItem* item : items())
        {
            if (item->type() > QGraphicsItem::UserType)
            {
                item->setFlag(QGraphicsItem::ItemIsSelectable, false);
                item->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
        }
        break;
    case Select:
        static_cast<QGraphicsView*>(views().at(0))->setDragMode(QGraphicsView::RubberBandDrag);
        for (QGraphicsItem* item : items())
            if (item->type() > QGraphicsItem::UserType)
                item->setFlags(QGraphicsItem::ItemIsSelectable);
        break;
    case InsertGenerator:
        insertedItem = new Generator(generators.size() + 1);
        break;
    case InsertKnot:
        insertedItem = new Knot(knots.size() + 1);
        break;
    case InsertResistor:
        insertedItem = new Resistor;
        break;
    case InsertLoad:
        insertedItem = new Load;
        break;
    default:
        break;
    }

    switch (mode)
    {
    case InsertGenerator:
    case InsertKnot:
    case InsertResistor:
    case InsertLoad:
        for (QGraphicsItem* item : items())
        {
            if (item->type() > QGraphicsItem::UserType)
            {
                item->setFlag(QGraphicsItem::ItemIsSelectable, false);
                item->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
        }
        insertedItem->setPos(-500, -500);
        addItem(insertedItem);
        break;
    case InsertWire:
        for (QGraphicsItem* item : items())
        {
            if (item->type() > QGraphicsItem::UserType)
            {
                item->setFlag(QGraphicsItem::ItemIsSelectable, false);
                item->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
        }
        break;
    default:
        break;
    }
}

void SchemeGraphics::turn()
{
    if (insertedItem)
    {
        QPoint correctPoint;
        switch(insertedItem->type())
        {
        case SchemeItem::TypeResist:
            insertedItem->setRotation(insertedItem->rotation() == 0 ? 90 : 0);
            correctPoint = QPoint(0, insertedItem->rotation() == 0 ? -1 : 1);
            insertedItem->setTransformOriginPoint(insertedItem->transformOriginPoint() + correctPoint);
            insertedItem->setPos(insertedItem->pos() - correctPoint);
            break;
        case SchemeItem::TypeLoad:
            switch(int(insertedItem->rotation()))
            {
            case 0:
                insertedItem->setRotation(90);
                correctPoint = QPoint(0, 1);
                break;
            case 90:
                insertedItem->setRotation(180);
                correctPoint = QPoint(1, 0);
                break;
            case 180:
                insertedItem->setRotation(270);
                correctPoint = QPoint(0, -1);
                break;
            case 270:
                insertedItem->setRotation(0);
                correctPoint = QPoint(-1, 0);
                break;
            default:
                break;
            }
            insertedItem->setTransformOriginPoint(insertedItem->transformOriginPoint() + correctPoint);
            insertedItem->setPos(insertedItem->pos() - correctPoint);
            break;
        default:
            break;
        }
    }
}

QPointF SchemeGraphics::gridPoint(const QPointF &point)
{
    qreal pos_x = grid_step * (int(point.x()) / grid_step) + grid_step / 2;
    qreal pos_y = grid_step * (int(point.y()) / grid_step) + grid_step / 2;

    return QPointF(pos_x, pos_y);
}

void SchemeGraphics::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsScene::mousePressEvent(event);

    switch (event->button())
    {
    case Qt::LeftButton:
        switch (mode)
        {
        case None:
            mouse_pressed = true;
            break;
        case InsertGenerator:
        case InsertKnot:
        case InsertResistor:
        case InsertLoad:
            if (!insertedItem->block)
            {
                switch (insertedItem->type())
                {
                case SchemeItem::TypeGen:
                    generators << insertedItem;
                    break;
                case SchemeItem::TypeKnot:
                    knots << insertedItem;
                    break;
                default:
                    break;
                }
                insertedItem->setTransparent(false);
                insertedItem = nullptr;
                setMode(mode);
            }
            break;

        case InsertWire:
            if (insertedItem) insertedItem->setTransparent(false);
            insertedItem = new Wire(gridPoint(event->scenePos()));
            addItem(insertedItem);
            break;

        default:
            break;
        }
        break;

    case Qt::RightButton:
        setMode(None);
        clearSelection();
        break;

    default:
        break;
    }
}

void SchemeGraphics::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsScene::mouseMoveEvent(event);
    bool block = false;
    switch (mode)
    {
    case None:
        if (mouse_pressed && mouseGrabberItem() != nullptr && mouseGrabberItem()->type() != SchemeItem::TypeWire)
            static_cast<SchemeItem*>(mouseGrabberItem())->setGridPos(event->scenePos());
        break;
    case InsertGenerator:
    case InsertKnot:
    case InsertResistor:
    case InsertLoad:
        insertedItem->setGridPos(event->scenePos());
        for (QGraphicsItem *item : insertedItem->collidingItems())
            if (item->type() > QGraphicsItem::UserType && item->type() != SchemeItem::TypeWire) block = true;
        insertedItem->setBlocked(block);
        break;

    case InsertWire:
        if (insertedItem) static_cast<Wire*>(insertedItem)->setP2(gridPoint(event->scenePos()));
        break;

    default:
        break;
    }
}

void SchemeGraphics::mouseReleaseEvent(QGraphicsSceneMouseEvent*) {
    mouse_pressed = false;
}