#include "vertex.h"

int Layout::quanityLayout=0;

Layout::Layout()
{
    quanityVertex = 0;
    quanityGener =  0;
    quanityLayout++;
}

Vertex *Layout::addVertex(complexnum load)
{
    QString index;
    Vertex* tempBufer = new Vertex;
    tempBufer->setId(++quanityVertex);
    tempBufer->setIndex(index.setNum(tempBufer->getId()));
    tempBufer->addLoad(complexnum(load));
    this->vertexes.insert(tempBufer, tempBufer->getId());
    return tempBufer;
}

Vertex* Layout::addVertex(QString index, complexnum load)
{
    Vertex* tempBufer = new Vertex;
    tempBufer->setIndex(index);
    tempBufer->addLoad(load);
    tempBufer->setId(++quanityVertex);
    this->vertexes.insert(tempBufer, tempBufer->getId());
    return tempBufer;
}

Gener* Layout::addGener(QString index)
{
    Gener* tempBufer = new Gener;
    tempBufer->setId(++quanityGener);
    if (index=="ID=index")
        index = "E" + index.setNum(quanityGener);
    tempBufer->setIndex(index);
    this->geners.insert(tempBufer, tempBufer->getId());
    this->GIndexes.insert(index, tempBufer);
    return tempBufer;
}

void Layout::addBranch(Node* node, Node* linkNode, complexnum condaction)
{
    node->addBranch(linkNode , condaction);
    linkNode->addBranch(node, condaction);
    QVector<Node*> temp(2);
    temp[0]=node;temp[1]=linkNode;
    this->branchs.insert(temp, condaction);
}

void Layout::setBranch(Node* node, Node* linkNode, complexnum condaction)
{
    node->setBranch(linkNode, condaction);
    linkNode->setBranch(node, condaction);
    QVector<Node*> temp(2);
    temp[0]=node;temp[1]=linkNode;
    if (this->branchs.contains(temp))
        this->branchs[temp]=condaction;
}

void Layout::deleteBranch(Node* node, Node* linkNode)
{
    node->deleteBranch(linkNode);
    linkNode->deleteBranch(node);
    QVector<Node*> temp(2);
    temp[0]=node;temp[1]=linkNode;
    this->branchs.remove(temp);
}

void Layout::addload(Node* node, complexnum load){node->addLoad(load);}

void Layout::setLoad(Node* node, int key, complexnum load){node->setLoad(key, load);}

void Layout::setVoltage(Node* node, double voltage){node->setVoltage(voltage);}

void Layout::setLoad(Node *node, complexnum load){node->setLoad(1,load);}

void Layout::deleteNode(Node* node)
{
    if (node->isVertex())
    {
        int id = node->getId();
        this->quanityVertex--;
        this->vertexes.remove(node);
        for (auto it = vertexes.begin(); it != vertexes.end(); ++it)
            if (it.key()->isLink(node))
                it.key()->deleteBranch(node);
        for (auto it = vertexes.begin(); it != vertexes.end(); ++it)
            if (it.value()>id)
            {
                this->vertexes[it.key()] = --it.value();
                it.key()->setId(it.value());
            }
    }
    else if (node->isGener())
    {
        int id = node->getId();
        this->quanityGener--;
        this->geners.remove(geners.key(node->getId()));
        for (auto it = geners.begin(); it != geners.end(); ++it)
            if (it.key()->isLink(node))
                it.key()->deleteBranch(node);
        for (auto it = geners.begin(); it != geners.end(); ++it)
            if (it.value()>id)
            {
                this->geners[it.key()] = --it.value();
                it.key()->setId(it.value());
            }
    }
}

int Layout::getId(Node* node){return node->getId();}

bool Layout::isVertex(Node *node)
{
    if (node->isVertex())
        return true;
    return false;
}

bool Layout::isGener(Node *node)
{
    if (node->isGener())
        return true;
    return false;
}

int Layout::getQuanityLayout(){return quanityLayout;}

int Layout::getQuanityVertex(){return this->quanityVertex;}

int Layout::getQuanityGener(){return this->quanityGener;}

int Layout::getLayoutId(){return this->layoutId;}

linksDict Layout::getLinks(){return this->branchs;}

const nodeDict& Layout::getVertexes(){return this->vertexes;}

generDict Layout::getGeners(){return this->geners;}

Gener* Layout::getGener(int id){return this->geners.key(id);}

Gener *Layout::getGener(QString index)
{
    return this->GIndexes.value(index);
}

Node* Layout::getVertex(int id){return this->vertexes.key(id);}

Node *Layout::operator[](int id)
{
    for (auto it = vertexes.begin(); it != vertexes.end(); ++it)
        if (it.value()==id)
            return it.key();
    id = id - this->quanityVertex;
    for (auto it = geners.begin(); it != geners.end(); ++it)
        if (it.value()==id)
            return it.key();
    return nullptr;
}

Layout::~Layout(){this->quanityLayout--;}






Node::Node(){}
Node::~Node(){}
int Node::getId(){return this->id;}

bool Node::isLink(Node *node)
{
    if (branchs.contains(node))
        return true;
    return false;
}

void Node::setVoltage(double voltage){voltage++;}

double Node::getVoltage(){return 0;}

QString Node::getIndex(){return this->index;}

loadDict Node::getLoads(){return this->loads;}

void Node::setIndex(QString index){this->index=index;}

void Node::setLoad(complexnum load)
{
    if (this->loads.contains(1))
        this->loads[1] = load;
}

void Node::addBranch(Node* linkNode, complexnum condaction)
{
    this->branchs.insert(linkNode, condaction);
    quanityBranch++;
}

void Node::setId(int id){this->id=id;}

branchDict Node::getBranchs(){return branchs;}

void Node::setBranch(Node *linkNode, complexnum condaction)
{
    if (this->branchs.contains(linkNode))
        this->branchs[linkNode] = condaction;
}

void Node::deleteBranch(Node* linkNode){this->branchs.remove(linkNode);}

complexnum Node::getCondacton(Node *linkNode){return this->branchs.value(linkNode, 0);}

complexnum Node::getCondacton(){return this->branchs.first();}

int Node::addLoad(complexnum load){this->loads.insert(++quanityLoad, complexnum(load)); return quanityLoad;}

complexnum Node::getLoad(int key){return this->loads.value(key, 0);}

void Node::setLoad(int key, complexnum load)
{
    if (this->loads.contains(key))
        this->loads[key] = load;
}

complexnum Node::getSumLoads()
{
    complexnum result;
    for (auto it=loads.begin();it!=loads.end();++it)
        result += it.value();
    return result;
}

complexnum Node::getSumCondactons()
{
    complexnum result;
    for (auto it=branchs.begin();it!=branchs.end();++it)
        result += it.value();
    return result;
}





Vertex::Vertex()
{
    quanityLoad = 0;
    quanityBranch = 0;
}

bool Vertex::isVertex(){return true;}

bool Vertex::isGener(){return false;}

Vertex::~Vertex()
{

}



Gener::Gener()
{
    quanityBranch = 0;
    voltage = 230;
}

void Gener::setVoltage(double voltage){this->voltage=voltage;}

double Gener::getVoltage(){return voltage;}

bool Gener::isVertex(){return false;}

bool Gener::isGener(){return true;}

int addLoad(complexnum load) = delete;

void sedLoad(int key,complexnum load) = delete;

complexnum getLoad(int key) = delete;

complexnum getSumLoads() = delete;

Gener::~Gener()
{

}

int Gener::getMatrIndexLink()
{
    return this->branchs.begin().key()->getId()-1;
}


