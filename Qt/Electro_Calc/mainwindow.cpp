#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QToolButton>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QActionGroup>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    schemeGraphics = static_cast<SchemeGraphics*>(ui->graphicsView->scene());
    ui->panelLayout->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
    //-------------------------------------------------------------------------------------
    QToolButton *choiceAction = new QToolButton;
    choiceAction->setCheckable(true);
    choiceAction->setChecked(true);
    choiceAction->setIcon(QIcon(":/res/pointer.png"));
    choiceAction->setIconSize(QSize(30, 30));
    ui->toolBar->addWidget(choiceAction);

    QToolButton *grabAction = new QToolButton;
    grabAction->setCheckable(true);
    grabAction->setIcon(QIcon(":/res/grab.png"));
    grabAction->setIconSize(QSize(30, 30));
    ui->toolBar->addWidget(grabAction);

    QToolButton *selectAction = new QToolButton;
    selectAction->setCheckable(true);
    selectAction->setIcon(QIcon(":/res/select.png"));
    selectAction->setIconSize(QSize(30, 30));
    ui->toolBar->addWidget(selectAction);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(ui->gridVisible);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(ui->acceptAction);

    modeButtonGroup = new QButtonGroup(this);
    //modeButtonGroup->setExclusive(false);
    modeButtonGroup->addButton(choiceAction, SchemeGraphics::None);
    modeButtonGroup->addButton(grabAction, SchemeGraphics::Grab);
    modeButtonGroup->addButton(selectAction, SchemeGraphics::Select);
    modeButtonGroup->addButton(ui->addGenerator, SchemeGraphics::InsertGenerator);
    modeButtonGroup->addButton(ui->addKnot, SchemeGraphics::InsertKnot);
    modeButtonGroup->addButton(ui->addResistor, SchemeGraphics::InsertResistor);
    modeButtonGroup->addButton(ui->addWire, SchemeGraphics::InsertWire);
    modeButtonGroup->addButton(ui->addLoad, SchemeGraphics::InsertLoad);

    connect(modeButtonGroup, QOverload<QAbstractButton*, bool>::of(&QButtonGroup::buttonToggled),
            this, &MainWindow::setMode);
    //-------------------------------------------------------------------------------------

    connect(ui->clearAction, &QAction::triggered, schemeGraphics, &SchemeGraphics::clearScene);
    connect(ui->deleteAction, &QAction::triggered, schemeGraphics, &SchemeGraphics::removeSelectedItems);
    connect(ui->turnAction, &QAction::triggered, schemeGraphics, &SchemeGraphics::turn);
    connect(ui->gridVisible, &QAction::triggered, ui->graphicsView, &GraphicsView::showGrid);
    connect(ui->acceptAction, &QAction::triggered, this, &MainWindow::calculateScheme);

    /*QActionGroup *dragActionGroup = new QActionGroup(this);
    dragActionGroup->addAction(ui->choiceAction);
    dragActionGroup->addAction(ui->grabAction);
    dragActionGroup->addAction(ui->selectAction);
    connect(ui->choiceAction, &QAction::toggled, ui->graphicsView, &GraphicsView::setChoiseMode);
    connect(ui->grabAction, &QAction::triggered, ui->graphicsView, &GraphicsView::setGrabMode);
    connect(ui->selectAction, &QAction::triggered, ui->graphicsView, &GraphicsView::setSelectMode);*/

    showMaximized();
}

void MainWindow::setMode(QAbstractButton *sender, bool checked)
{
    /*modeButtonGroup->blockSignals(true);
    for (QAbstractButton *button : modeButtonGroup->buttons())
    {
        if (button == sender)
        {
            ui->choiceAction->setChecked(true);
            if (checked) schemeGraphics->setMode(SchemeGraphics::Mode(modeButtonGroup->id(sender)));
        }
        else (button->setChecked(false));
    }
    schemeGraphics->clearSelection();
    modeButtonGroup->blockSignals(false);*/
    if (checked) schemeGraphics->setMode(SchemeGraphics::Mode(modeButtonGroup->id(sender)));
}

void MainWindow::calculateScheme()
{
    ui->scheme->setScene(schemeGraphics);
    ui->stackedWidget->setCurrentWidget(ui->calculateWidget);

    ui->scheme->setSceneRect(schemeGraphics->itemsBoundingRect());
    ui->scheme->setMinimumHeight(ui->scheme->sceneRect().height() + 20);

    if (ui->scheme->sceneRect().width() > ui->scheme->width())
    {
        ui->scheme->fitInView(schemeGraphics->itemsBoundingRect(), Qt::KeepAspectRatio);
        ui->scheme->setMinimumHeight(ui->scheme->scene()->itemsBoundingRect().height() *
                                     ui->scheme->width() / ui->scheme->scene()->itemsBoundingRect().width());
    }

    Layout layout;
    for (QGraphicsItem *item : schemeGraphics->items())
    {
        if (item->type() == SchemeItem::TypeGen)
            layout.setVoltage(layout.addGener(QString("E%1").arg(static_cast<Generator*>(item)->id)), static_cast<Generator*>(item)->data.toDouble());
        else if (item->type() == SchemeItem::TypeKnot)
        {
            for (QGraphicsItem *collideItem : item->collidingItems())
            {
                if (collideItem->type() == SchemeItem::TypeWire &&
                        (item->contains(static_cast<Wire*>(collideItem)->line.p1()) ||
                         item->contains(static_cast<Wire*>(collideItem)->line.p2())))
                {
                    ;
                }
            }
        }
    }
}

void MainWindow::findItem(Wire *wire)
{
    for (QGraphicsItem *item : wire->collidingItems())
    {
        switch (item->type())
        {
        case SchemeItem::TypeWire:
            if (item->contains(wire->line.p1()) ||
                item->contains(wire->line.p2()) ||
                wire->contains(static_cast<Wire*>(item)->line.p1()) ||
                wire->contains(static_cast<Wire*>(item)->line.p2()))
                findItem(static_cast<Wire*>(item));
            break;
        }

    }
}

void MainWindow::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::RightButton)
    {
        modeButtonGroup->button(SchemeGraphics::None)->setChecked(true);
        //for (QAbstractButton *button : modeButtonGroup->buttons()) button->setChecked(false);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Delete) schemeGraphics->removeSelectedItems();
}
