#ifndef VERTEX_H
#define VERTEX_H
#include <math.h>
#include <complex>
#include <QMultiMap>
#include <QVector>

class Layout;
class Vertex;
class Gener;
class Node;
class Calculation;

typedef QMap<QVector<Node*>, std::complex<double>> linksDict;
typedef QMap<Node*, std::complex <double>> branchDict;
typedef QMap<int, std::complex <double>> loadDict;
typedef QMap<Node*, int> nodeDict;
typedef QMap<Gener*, int> generDict;
typedef QMap<QString, Gener*> generIndexes;
typedef std::complex<double> complexnum;

class Layout
{
    friend Calculation;
public:
    Layout();
    Gener* addGener(QString index="ID=index");
    Vertex* addVertex(complexnum load=1);
    Vertex* addVertex(QString index, complexnum load=1);
    void addload(Node* node, complexnum load=1);
    void addBranch(Node* node, Node* linkNode, complexnum condaction=1);

    void setVoltage(Node* node, double voltage);
    void setLoad(Node* node, complexnum load);
    void setLoad(Node* node,int key, complexnum load);
    void setBranch(Node* node, Node* linkNode, complexnum condaction);

    void deleteBranch(Node* node, Node* linkNode);
    void deleteNode(Node* node);

    int getId(Node* node);

    bool isVertex(Node* node);
    bool isGener(Node* node);

    static int getQuanityLayout();
    int getQuanityVertex();
    int getQuanityGener();
    int getLayoutId();
    linksDict getLinks();
    const nodeDict& getVertexes();
    generDict getGeners();
    Gener* getGener(int id);
    Gener* getGener(QString index);
    Node* getVertex(int id);
    Node* operator[](int id);
    ~Layout();
private:
    int layoutId;
    int quanityGener;
    int quanityVertex;
    linksDict branchs;
    nodeDict vertexes;
    generDict geners;
    generIndexes GIndexes;
    static int quanityLayout;
};




class Node
{
    friend Layout;
    friend Calculation;
public:
    Node();
    virtual int getId();
    virtual bool isGener()=0;
    virtual bool isVertex()=0;
    virtual bool isLink(Node* node);
    virtual void setVoltage(double voltage);
    virtual double getVoltage();
    virtual QString getIndex();
    virtual loadDict getLoads();
    virtual branchDict getBranchs();
    virtual complexnum getSumLoads();
    virtual complexnum getSumCondactons();
    virtual complexnum getLoad(int key=1);
    virtual complexnum getCondacton(Node* linkNode);
    virtual complexnum getCondacton();
    ~Node();
protected:
    virtual int addLoad(complexnum load=1);
    virtual void setIndex(QString index);
    virtual void setLoad(complexnum load);
    virtual void deleteBranch(Node* linkNode);
    virtual void setLoad(int key, complexnum load);
    virtual void setBranch(Node* linkNode, complexnum condaction);
    virtual void addBranch(Node* linkNode, complexnum condaction=1);
    virtual void setId(int id);
    int id;
    int quanityLoad;
    int quanityBranch;
    QString index;
    loadDict loads;
    branchDict branchs;
};


class Vertex : public Node
{
public:
    Vertex();
    bool isVertex() override;
    bool isGener() override;
    ~Vertex();
};

class Gener : public Node
{
public:
    Gener();
    bool isVertex() override;
    bool isGener() override;
    void setVoltage(double voltage) override;
    double getVoltage() override;
    int getMatrIndexLink();
    ~Gener();
private:

    double voltage;
};

#endif // VERTEX_H
