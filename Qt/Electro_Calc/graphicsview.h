#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    GraphicsView(QWidget* = 0);

private:
    QPixmap texture;

protected:
    void wheelEvent(QWheelEvent*);

public slots:
    //void setChoiseMode(bool);
    //void setGrabMode();
    //void setSelectMode();
    void showGrid(bool);
};

#endif // GRAPHICSVIEW_H
