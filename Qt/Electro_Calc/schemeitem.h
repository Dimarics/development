#ifndef SCHEMEITEM_H
#define SCHEMEITEM_H

#include <QGraphicsItem>
#include <QInputDialog>

class SchemeItem;
class SelectionGlow;
class Generator;
class Knot;
class Load;
class Branch;
//--------------------------------------------------------------------------

class SchemeItem : public QGraphicsPathItem
{
public:
    enum
    {
        SchemeType = QGraphicsItem::UserType,
        TypeGen,
        TypeKnot,
        TypeLoad,
        TypeResist,
        TypeWire
    };
    QString data = "200";

    const QColor pen_color = QColor(132, 0, 0);
    const QColor text_color = QColor(0, 132, 132);

    bool block;
    uint id;

    SchemeItem(const uint = 1);
    void setId(uint);
    void setBlocked(bool);
    void setTransparent(bool);
    void setGridPos(const QPointF&);
    int type() const;

private:
    qreal opacity;
    SelectionGlow *selectionGlow;

protected:
    QString string_id;
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
};
//--------------------------------------------------------------------------
class SelectionGlow : public QGraphicsItem
{
public:
    SelectionGlow(QGraphicsItem*);

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
    QRectF boundingRect() const;
};
//--------------------------------------------------------------------------
class Generator : public SchemeItem
{
public:
    Generator(uint);
    int type() const;

private:
    QRectF rect;

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
};
//--------------------------------------------------------------------------
class Knot : public SchemeItem
{
public:
    Knot(uint);
    int type() const;

private:
    QRectF rect;

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
};
//--------------------------------------------------------------------------
class Resistor : public SchemeItem
{
public:
    Resistor();
    int type() const;

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
};
//--------------------------------------------------------------------------
class Wire : public SchemeItem
{
public:
    Wire(const QPointF&);
    int type() const;
    QLineF line;

    void setP2(const QPointF&);
    void pointMode(const QPointF&);

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
};
//--------------------------------------------------------------------------
class Load : public SchemeItem
{
public:
    qreal load;
    Load();
    int type() const;

private:
    const qreal width = 15;
    const qreal height = 100;
    QPolygonF polygon;

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*);
};
//--------------------------------------------------------------------------
#endif // SCHEMEITEM_H
