#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "drawgraphics.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* = 0);
    ~MainWindow();

private:
    uint period;
    //uint time_rate;
    uint time_point = 0;
    QList<QSerialPortInfo> port_list;
    QSerialPort *serial_port;
    DrawGraphics *drawGraphics[3];
    Ui::MainWindow *ui;
    qreal average(const QVector<qreal>&);

private slots:
    void setConfig();
    void open();
    void saveAs();
    void saveConfig();
    void fullScreen(bool);
    void settingsView(bool);
    void portChanged(const QString&);
    void checkPorts();
    void readPort();
    void chartChanged(quint8);
    void setColor();
    void start();
    void stop();
    void scrollBegin();
    void scrollEnd();

    void setTimeStep(QTime);
    void setPeriod(quint16);
    void setStretch(quint8);
    void setMinY(int);
    void setMaxY(int);
    void setCountY(int);

    void hideVoltage(bool);
    void hideAmperage(bool);
    void hidePower(bool);
};
#endif // MAINWINDOW_H
