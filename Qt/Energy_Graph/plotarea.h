#ifndef PLOTAREA_H
#define PLOTAREA_H

#include <QGraphicsItem>
#include <QPainter>
#include <QTime>
#include <QGraphicsSceneEvent>
#include <QToolTip>

class PlotArea;
class ToolZone;

class PlotArea : public QGraphicsItem
{
public:
    QColor color;
    QByteArray demens;
    QTime start_time;
    QTime time_step;
    quint16 width = 0;
    quint16 height = 0;
    quint16 bounding_width = 0;
    quint16 stretch_x = 100;
    qreal min_y = 0;
    qreal max_y = 32;
    QList<QPointF> points;
    QList<ToolZone*> tool_tips;
    PlotArea();
    void addPoint(qreal, quint32);

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
    QRectF boundingRect() const;
};

class ToolZone : public QGraphicsItem
{
public:
    bool draw_point = false;
    QColor color;
    ToolZone(const QColor&, QGraphicsItem* = 0);

protected:
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
    void hoverEnterEvent(QGraphicsSceneHoverEvent*);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent*);
    QRectF boundingRect() const;
};

#endif // PLOTAREA_H
