#include <QGraphicsTextItem>
#include <QGraphicsProxyWidget>
#include "drawgraphics.h"

#define MARGIN 50

DrawGraphics::DrawGraphics(QWidget *parent) : QGraphicsView(parent)
{
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setStyleSheet("QGraphicsView {border: none}");
    setMinimumSize(100, 150);

    scene = new QGraphicsScene;
    plotArea = new PlotArea;
    scene->addItem(plotArea);
    plotArea->setPos(60, MARGIN); // w
    plotArea->bounding_width = 600;
    setScene(scene);

    scrollBar = new QScrollBar(this);
    scrollBar->setOrientation(Qt::Horizontal);
    //scrollBar->setMinimum(0);
    scrollBar->close();
    connect(scrollBar, &QAbstractSlider::sliderMoved, this, &DrawGraphics::scrollGraphic);
}

void DrawGraphics::drawForeground(QPainter *painter, const QRectF&)
{
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::white);
    painter->drawRect(0, 0, 59, plot_height); // Левая граница
    painter->drawRect(0, 0, width(), MARGIN - 1); // Верхняя граница
    painter->drawRect(width() - 50, 0, 50, plot_height); // Правая граница

    QPen pen = QPen(Qt::lightGray);
    pen.setWidth(2);
    painter->setPen(pen);
    painter->drawLine(55, plot_height - 40, width() - 46, plot_height - 40); //ось x
    painter->drawLine(60, MARGIN - 4, 60, plot_height - 40); //ось y

    qreal space = qreal(plot_height - MARGIN - 40) / count_y;
    for (quint8 i = 0; i <= count_y; ++i) // Значения оси y
    {
        painter->setPen(Qt::lightGray);
        painter->drawLine(55, MARGIN + i * space, 60, MARGIN + i * space);
        painter->setPen(Qt::black);
        qreal val = plotArea->max_y - ((plotArea->max_y - plotArea->min_y) / count_y) * i;
        bool dec = val == 0 ? 0 : 1;
        QByteArray value = QByteArray::number(val, 'f', dec);
        quint16 font_pos_x = painter->fontMetrics().size(Qt::TextSingleLine, value).width();
        painter->drawText(50 - font_pos_x, MARGIN + 5 + i * space, value);
    }

    if (scrollBar->value() == 0)
    {
        QString start_time = plotArea->start_time.toString("hh:mm");
        quint16 font_width = painter->fontMetrics().boundingRect(start_time).width() / 2;
        painter->drawText(60 - font_width, plot_height - 19, start_time);
    }

    painter->setPen(Qt::black); // Заголовок
    QFont font = painter->font();
    font.setPointSize(13);
    painter->setFont(font);
    quint16 font_pos_x = QFontMetrics(font).boundingRect(header).width();
    painter->drawText((width() - font_pos_x) / 2, MARGIN / 2 + 7, header);
}

void DrawGraphics::drawBackground(QPainter *painter, const QRectF&)
{
    painter->setPen(Qt::lightGray);
    qreal space = qreal(plot_height - MARGIN - 40) / count_y;
    for (quint8 i = 0; i < count_y; ++i)
        painter->drawLine(61, MARGIN + i * space, width() - 50, MARGIN + i * space);
}

void DrawGraphics::setScrollBar()
{
    bool scroll_to_end = scrollBar->value() < scrollBar->maximum() - 5 ? false : true;
    scrollBar->setMaximum(plotArea->bounding_width - width() + 110);
    scrollBar->setMinimum(0);
    scrollBar->setPageStep(scrollBar->maximum() / 2.5);
    if (plotArea->bounding_width + 60 <= width() && scrollBar->isVisible())
    {
        scrollBar->setValue(0);
        scrollBar->close();
        plot_height += 20;
        plotArea->height += 20;
    }
    else if (plotArea->bounding_width + 60 > width() && !scrollBar->isVisible())
    {
        scrollBar->setValue(scrollBar->maximum());
        scrollBar->show();
        plotArea->height -= 20;
        plot_height -= 20;
    }
    if (scroll_to_end)
    {
        scrollBar->setValue(scrollBar->maximum());
        scrollGraphic(scrollBar->maximum());
    }
}

void DrawGraphics::updateView()
{
    repaint();
    setScrollBar();
}

void DrawGraphics::resizeEvent(QResizeEvent*)
{
    plot_height = scrollBar->isVisible() ? height() - 20 : height();
    plotArea->width = width() - 60;
    plotArea->height = plot_height - MARGIN - 41;
    scene->setSceneRect(0, 0, width(), height());
    scrollBar->setGeometry(0, height() - 20, width(), 20);
    updateView();
}

void DrawGraphics::scrollGraphic(int value)
{
    plotArea->setPos(-value + 60, MARGIN);
    update();
}

void DrawGraphics::setColor(const QColor &color)
{
    plotArea->color = color;
    for (ToolZone *toolZone : plotArea->tool_tips)
        toolZone->color = color;
}

void DrawGraphics::wheelEvent(QWheelEvent *event)
{
    qint8 step = (event->angleDelta().y() > 0 ? -scrollBar->maximum() : scrollBar->maximum()) / 12;
    quint16 value = scrollBar->value() + step < 0 ? 0 : scrollBar->value() + step;
    scrollBar->setValue(value);
    scrollGraphic(value);
}