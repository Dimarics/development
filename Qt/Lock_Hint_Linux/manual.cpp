#include "manual.h"
#include "settings.h"

Manual::Manual(QWidget *parent) : QTextBrowser (parent)
{
    //int x = 1260/2;
    setStyleSheet(Style::manual);
    QPushButton *backButton = new QPushButton(tr("Назад"), this);
    QLabel *arrow = new QLabel(backButton);
    arrow->setPixmap(QPixmap (":/res/backBtn.png"));
    arrow->setGeometry(12, 10, 20, 20);
    backButton->setStyleSheet(Style::backBtn);
    backButton->resize(116, 40);
    connect(backButton, &QPushButton::clicked, this, &QWidget::close);
    QString text
    {
        //"<div style=\"margin-left: 200px\">"
        "<p align=\"center\"><h3><font color=\"#4CB0FF\">Справочник</font></h3>"
        "<h4><font color=\"#FFFF99\">Общие</font></h4></p>"
        "<blockquote><ul type=\"circle\">"
        "<li><p><b><i>Клавиша \"Esc\" </i></b>"
        "- выполняет переход в предыдущее меню.</p></li>"
        "<li><p><b><i>Индикатор раскладки </i></b>"
        "- отображает текущий язык раскладки клавиатуры.</p></li>"
        "<li><p><b><i>Кнопка видимости пароля </i></b>"
        "- показывает либо скрывает пароль.<br>Комбинация клавиш: Alt + S.</p></li>"

        "<p align=\"center\"><h4><font color=\"#FFFF99\"><br>Главное меню</font></h4></p>"
        "<li><p><b><i>Таблица данных </i></b>"
        "- служит для отображения сервисов. Клик левой кнопкой мыши по ячейке приведёт к её выделению. "
        "Нажатие правой кнопкой мыши вызовет контекстное меню. Сервисы можно удалять клавишей \"Delete\". "
        "Копирование содержимого ячейки производится сочетанием клавиш Ctrl+С после её выделения, "
        "либо из контекстного меню. К сервисам можно добавлять ссылки для перехода на сайт. "
        "Клавиша \"Alt\" очищает выбор ячеек, длительное нажатие включает режим перехода по ссылкам."
        "</p></li>"
        "<li><p><b><i>Меню \"параметры\" </i></b>"
        "- предназначено для редактирования настроек.<br>Комбинация клавиш: Alt + P.</p></li>"
        "<li><p><b><i>Кнопка \"добавить\" </i></b>"
        "- служит для добавления новых сервисов.<br>Комбинация клавиш: Alt + A.</p></li>"
        "<li><p><b><i>Кнопка блокировки </i></b>"
        "- включает меню блокировки.<br>Клавиша \"Esc\".</p></li>"
        "</ul></blockquote>"
    };
    setText(text);
}
