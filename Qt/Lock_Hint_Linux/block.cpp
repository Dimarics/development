#include "block.h"
#include "settings.h"
#include <QTextStream>

BlockMenu::BlockMenu(QWidget *parent) : QWidget (parent)
{
    int x = 1260/2;

    QLabel *lock = new QLabel(this);
    lock->setPixmap(QPixmap(":/res/lock.png"));
    lock->setGeometry(x - 96, 150, 192, 300);

    string = new PasswordLine(this);
    string->setGeometry(x - 260, 550, 520, 45);
    string->setStyleSheet(Style::stringPassword);
    string->setAlignment(Qt::AlignmentFlag::AlignCenter);
    NoCursorStyle *noFocus = new NoCursorStyle();
    string->setStyle(noFocus);
    connect(string, &QLineEdit::returnPressed, this, &BlockMenu::enterPassword);
    connect(string, &QLineEdit::selectionChanged, this, &BlockMenu::textDeselect);

    btn_vision = new QPushButton(this);
    btn_vision->setGeometry(x - 307, 550, 45, 45);
    btn_vision->setStyleSheet(Style::visionBlockPassword);
    btn_vision->setCheckable(true);
    btn_vision->setShortcut(tr("Alt+S"));
    btn_vision->setCursor(Qt::PointingHandCursor);
    connect(btn_vision, &QPushButton::toggled, string, &PasswordLine::showPassword);
    connect(btn_vision, &QPushButton::toggled, this, &BlockMenu::toolTip);
    toolTip(true);

    layout = new QLabel(this);
    layout->setGeometry(x + 262, 550, 45, 45);
    layout->setStyleSheet(Style::layoutFirst);
    layout->setFont(QFont("Algerian", 21));
    layout->setAlignment(Qt::AlignmentFlag::AlignCenter);

    passwordError = new QLabel(this);
    passwordError->setText("Пароль неверный!");
    passwordError->setAlignment(Qt::AlignmentFlag::AlignHCenter);
    passwordError->setStyleSheet(Style::passwordError);
    passwordError->setGeometry(x-150, 620, 300, 40);
    passwordError->close();
    errTime = new QTimer();
    errTime->setInterval(2000);
    connect(errTime, &QTimer::timeout, passwordError, &QLabel::close);

    if (database.isEmpty())
    {
        hint = new QLabel(this);
        hint->setText("Придумайте пароль (не более 26 символов)");
        hint->setAlignment(Qt::AlignmentFlag::AlignHCenter);
        hint->setStyleSheet(Style::blockHint);
        hint->setGeometry(x-250, 620, 500, 40);
    }
}

void BlockMenu::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QColor(0, 0, 0, 0)); //фон виджета
    painter.setBrush(QColor(45, 45, 45, 255));
    painter.drawRect(rect());
}

void BlockMenu::switchLayout(bool lang)
{
    if (lang)
        layout->setPixmap(QPixmap(":/res/ru_block.png"));
    else
        layout->setPixmap(QPixmap(":/res/en_block.png"));
}

void BlockMenu::enterPassword()
{
    if (database.isEmpty())
    {
        database << string->text();
        emit block(false);
        hint->~QLabel();
        string->setText("");
        btn_vision->setChecked(false);
        cryptography();
    }
    else if (string->text() == database.at(0))
    {
        emit block(false);
        string->clear();
        btn_vision->setChecked(false);
    }
    else
    {
        passwordError->show();
        errTime->start();
    }
}

void BlockMenu::textDeselect()
{
    string->deselect();
}

void BlockMenu::toolTip(bool show)
{
    if (show)
        btn_vision->setToolTip("Показать пароль");
    else
        btn_vision->setToolTip("Скрыть пароль");
}
