#ifndef TABLE_H
#define TABLE_H

#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QWheelEvent>
#include <QMenu>
#include <QApplication>
#include <QClipboard>

extern int text_color;

class Table : public QTableWidget
{
    Q_OBJECT

public:
    QMenu *option;
    QAction *edit;
    QAction *del;
    Table(QWidget *parent = 0);

protected:
    void focusOutEvent(QFocusEvent *event);
    void wheelEvent(QWheelEvent* e);

private slots:
    //void stopScroll(int);
    void contextMenu(QPoint);
    void copyText();
};

class TableDelegate : public QStyledItemDelegate
{
protected:
    void paint(QPainter* painter, const QStyleOptionViewItem &option, const QModelIndex& index) const;
};

#endif // TABLE_H
