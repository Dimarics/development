#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QMouseEvent>
#include <QPushButton>
#include <QTimer>

#include "block.h"
#include "menu.h"

void decryption();

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);

private:
    QPoint mp;
    BlockMenu *block_menu;
    MainMenu *main_menu;
    QPalette blockStyle;
    bool window_move = false;
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private slots:
    void checkLang();

public slots:
    void setMenu(bool);

signals:
    void switched(bool);
};
#endif // WINDOW_H
