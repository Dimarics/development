#ifndef MANUAL_H
#define MANUAL_H

#include <QTextBrowser>
#include <QPushButton>
#include <QLabel>

class Manual : public QTextBrowser
{
    Q_OBJECT

public:
    Manual(QWidget *parent = 0);
};

#endif // MANUAL_H
