#ifndef EDITOR_H
#define EDITOR_H

#include <QWidget>
#include <QPainter>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QKeyEvent>

class EditWidget : public QWidget
{
    Q_OBJECT

public:
    QPushButton *btn_ok;
    QLineEdit *service_enter;
    QLineEdit *login_enter;
    QLineEdit *password_enter;
    QLineEdit *link_enter;
    EditWidget(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
};

#endif // EDITOR_H
