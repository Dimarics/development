#include "menu.h"
#include "settings.h"
#include <QDebug>

MainMenu::MainMenu(QWidget *parent) : QWidget (parent)
{
    int x = 1260/2;

    table = new Table(this); //таблица данных
    fillTable();
    connect(table, &QTableWidget::cellDoubleClicked, this, &MainMenu::replaceContent);
    connect(table, &QTableWidget::cellClicked, this, &MainMenu::followLink);
    connect(table, &QTableWidget::cellEntered, this, &MainMenu::linkSelect);
    connect(table->edit, &QAction::triggered, this, &MainMenu::addContent);
    connect(table->del, &QAction::triggered, this, &MainMenu::deleteContent);

    editor = new EditWidget(this);
    editor->setGeometry(x-261, 95, 670, 395);
    editor->installEventFilter(this);
    QGraphicsDropShadowEffect *shadow_1 = new QGraphicsDropShadowEffect(this);
    shadow_1->setColor(QColor(0, 0, 0, 255));
    shadow_1->setBlurRadius(15);
    shadow_1->setOffset(0);
    editor->setGraphicsEffect(shadow_1);
    editor->close();
    connect(editor->btn_ok, &QPushButton::clicked, this, &MainMenu::newContent);

    QPushButton *btn_parameters = new QPushButton(this); //параметры
    btn_parameters->setStyleSheet(Style::panelBtn);
    btn_parameters->setGeometry(x - 306, 50, 45, 45);
    btn_parameters->setIcon(QPixmap(":/res/parameters.png"));
    btn_parameters->setIconSize(QSize(31, 31));
    btn_parameters->setCursor(Qt::PointingHandCursor);
    btn_parameters->setShortcut(tr("Alt+P"));
    btn_parameters->setToolTip("Параметры");
    QMenu *parameters = new QMenu(this);
    parameters->setStyleSheet(Style::contextMenu);
    QAction *editPassword = new QAction("Изменить пароль", this);
    parameters->addAction(editPassword);
    connect(editPassword, &QAction::triggered, this, &MainMenu::newPassword);
    parameters->addSeparator();
    QAction *help = new QAction("Справочник", this);
    parameters->addAction(help);
    connect(help, &QAction::triggered, this, &MainMenu::openManual);
    btn_parameters->setMenu(parameters);

    QPushButton *btn_add = new QPushButton(this); //добавление сервиса
    btn_add->setStyleSheet(Style::panelBtn);
    btn_add->setGeometry(x - 262, 50, 45, 45);
    btn_add->setIcon(QPixmap(":/res/addBtn.png"));
    btn_add->setIconSize(QSize(31, 31));
    btn_add->setCursor(Qt::PointingHandCursor);
    btn_add->setShortcut(tr("Alt+A"));
    btn_add->setToolTip("Добавить");
    connect(btn_add, &QPushButton::clicked, this, &MainMenu::addContent);

    search = new QLineEdit(this);
    search->setStyleSheet(Style::searchBar); //строка поиска
    search->setGeometry(x - 217, 50, 434, 45);
    search->setPlaceholderText("Поиск");
    QLabel *searchIcon = new QLabel(search);
    searchIcon->setPixmap(QPixmap(":/res/search.png"));
    searchIcon->setGeometry(12, 10, 27, 27);
    searchClearButton = new QPushButton(search);
    searchClearButton->setStyleSheet(Style::clearTextBtn);
    searchClearButton->setCursor(Qt::PointingHandCursor);
    searchClearButton->setGeometry(405, 14, 19, 19);
    searchClearButton->close();
    connect(searchClearButton, &QPushButton::clicked, this, &MainMenu::searchClear);
    connect(search, &QLineEdit::textEdited, this, &MainMenu::tableSearch);

    layout = new QLabel(this);
    layout->setStyleSheet(Style::layoutSecond); //раскладка
    layout->setGeometry(x + 216, 50, 45, 45);
    layout->setAlignment(Qt::AlignmentFlag::AlignCenter);

    QPushButton *btn_block = new QPushButton(this); //кнопка блокировки
    btn_block->setStyleSheet(Style::panelBtn);
    btn_block->setGeometry(x + 260, 50, 45, 45);
    btn_block->setIcon(QPixmap(":/res/blockBtn.png"));
    btn_block->setIconSize(QSize(31, 31));
    btn_block->setCursor(Qt::PointingHandCursor);
    //btn_block->setShortcut(tr("Alt+B"));
    btn_block->setToolTip("Блокировка");
    connect(btn_block, &QPushButton::clicked, this, &MainMenu::blockMenu);

    passwordEditor = new PasswordEdit(this);
    passwordEditor->setGeometry(x-325, 269, 650, 242);
    QGraphicsDropShadowEffect *shadow_2 = new QGraphicsDropShadowEffect(this);
    shadow_2->setColor(QColor(0, 0, 0, 255));
    shadow_2->setBlurRadius(17);
    shadow_2->setOffset(0);
    passwordEditor->setGraphicsEffect(shadow_2);
    passwordEditor->close();
    connect(passwordEditor->btn_ok, &QPushButton::clicked, this, &MainMenu::editingCompleted);
    connect(passwordEditor->passwordLine, &QLineEdit::returnPressed, this, &MainMenu::editingCompleted);
    connect(passwordEditor->btn_close, &QPushButton::clicked, this, &MainMenu::closePasswordEditor);

    manual = new Manual(this);
    manual->setGeometry(0, 0, x*2-6, 794);
    manual->close();
}

void MainMenu::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);

    painter.setPen(QColor(0, 0, 0, 0)); //фон виджета
    painter.setBrush(QColor(33, 40, 48));
    painter.drawRect(rect());
}

void MainMenu::switchLayout(bool lang) //отображение текущей раскладки
{
    if (lang)
        layout->setPixmap(QPixmap(":/res/ru_menu.png"));
    else
        layout->setPixmap(QPixmap(":/res/en_menu.png"));
}

void MainMenu::blockMenu() //блокировка
{
    emit block(true);
    close();
}

void MainMenu::fillTable() //заполнение таблицы данными с листа
{
    int list_size = database.size()/4;
    if (list_size <= 10)
    {
        table->setGeometry(130, 140, 1003, 606);
        table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
    else
    {
        table->setGeometry(120, 140, 1021, 606);
        table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        table->setRowCount(list_size);
    }
    for (int i = 0; i < list_size; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            table->setItem(i, j, new QTableWidgetItem(database.at(i*4+j+1)));
        }
    }
    table->sortByColumn(0, Qt::AscendingOrder);
}

void MainMenu::tableSearch(QString text) //поиск по таблице
{
    if(text.isEmpty())
    {
        fillTable();
        searchClearButton->close();
    }
    else
    {
        int row_count = 0;
        searchClearButton->show();
        table->clearContents();
        for (int i = 0; i < database.size()/4; i++)
        {
            int found = database.at(i*4+1).indexOf(text, 0, Qt::CaseInsensitive);
            if (found != -1)
            {
                ++row_count;
                for(int j = 0; j < 4; j++)
                    table->setItem(i, j, new QTableWidgetItem(database.at(i*4+j+1)));
            }
        }
        if (row_count <= 10)
        {
            table->setRowCount(10);
            table->setGeometry(130, 140, 1003, 606);
            table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        }
        else
        {
            table->setRowCount(row_count);
            table->setGeometry(120, 140, 1021, 606);
            table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        }
        table->sortByColumn(0, Qt::AscendingOrder);
    }
}

void MainMenu::searchClear() //очистка строки поиска
{
    search->clear();
    search->setFocus();
    searchClearButton->close();
    fillTable();
}

void MainMenu::replaceContent(int i, int j) //изменение данных
{
    del = true;
    row_num = i;
    if ((table->item(i, 0) && table->item(i, 1) && table->item(i, 2) && table->item(i, 3)) == 0)
        addContent();
    else
    {
        editor->service_enter->setText(table->item(i, 0)->text());
        if (j == 0)
            editor->service_enter->setFocus();
        editor->login_enter->setText(table->item(i, 1)->text());
        if (j == 1)
            editor->login_enter->setFocus();
        editor->password_enter->setText(table->item(i, 2)->text());
        if (j == 2)
            editor->password_enter->setFocus();
        editor->link_enter->setText(table->item(i, 3)->text());
        editor->show();
    }
}

void MainMenu::addContent() //меню редатора
{
    del = false;
    table->setCurrentCell(0, 3);
    table->clearSelection();
    editor->show();
    editor->service_enter->clear();
    editor->service_enter->setFocus();
    editor->login_enter->clear();
    editor->password_enter->clear();
    editor->link_enter->clear();
}

void MainMenu::newContent() //добавление сервиса
{
    if (!search->text().isEmpty())
        searchClear();
    if (!del)
    {
        row_num = 0;
        if (!search->text().isEmpty())
            search->clear();
        while (table->item(row_num, 0) != 0)
            ++row_num;
        if (row_num > 9)
        {
            table->insertRow(row_num);
            if (row_num == 10)
            {
                table->setGeometry(120, 140, 1021, 606);
                table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
            }
        }
    }
    table->setItem(row_num, 0, new QTableWidgetItem(editor->service_enter->text()));
    table->setItem(row_num, 1, new QTableWidgetItem(editor->login_enter->text()));
    table->setItem(row_num, 2, new QTableWidgetItem(editor->password_enter->text()));
    table->setItem(row_num, 3, new QTableWidgetItem(editor->link_enter->text()));

    if (table->item(row_num, 0)->text().isEmpty() && table->item(row_num, 1)->text().isEmpty() &&
            table->item(row_num, 2)->text().isEmpty() && table->item(row_num, 3)->text().isEmpty())
    {
        clearRow(row_num);
    }
    table->sortByColumn(0, Qt::AscendingOrder);
    writeList();
    editor->close();
}

void MainMenu::clearRow(int row_num) //удаление сервиса
{
    if (table->rowCount() < 11)
    {
        table->takeItem(row_num, 0);
        table->takeItem(row_num, 1);
        table->takeItem(row_num, 2);
        table->takeItem(row_num, 3);
    }
    else
        table->removeRow(row_num);
    if (table->rowCount() == 10)
    {
        table->setGeometry(130, 140, 1003, 606);
        table->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
}

void MainMenu::deleteContent()
{
    clearRow(table->currentRow());
    table->sortByColumn(0, Qt::AscendingOrder);
    writeList();
}

void MainMenu::writeList() //запись данных в лист
{
    database.erase(database.begin() + 1, database.end());
    for (int i = 0; i < table->rowCount(); i++)
    {
        for (int j = 0; j < 4 && table->item(i, j) != 0; j++)
        {
            database << table->item(i, j)->text();
        }
    }
    cryptography();
}

void MainMenu::newPassword() //открытие редактора пароля
{
    passwordEditor->show();
    passwordEditor->passwordLine->setFocus();
}

void MainMenu::editingCompleted() //завершение редактирования пароля
{
    passwordEditor->close();
    database[0] = passwordEditor->passwordLine->text();
    passwordEditor->passwordLine->showPassword(false);
    writeList();
}

void MainMenu::closePasswordEditor() //закрытие редактора пароля
{
    passwordEditor->close();
    setFocus();
}

void MainMenu::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Alt)
    {
        alt_key_pressed = true;
        table->setCurrentCell(0, 3);
        table->clearSelection();
    }
    else if (event->key() == Qt::Key_Delete)
        deleteContent();
    else if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
    {
        if (passwordEditor->isVisible())
        {
            editingCompleted();
            if (editor->isVisible())
                editor->setFocus();
        }
    }
    else if (event->key() == Qt::Key_Escape)
    {
        if (manual->isVisible())
        {
            manual->close();
            if (editor->isVisible())
                editor->setFocus();
        }
        else if (passwordEditor->isVisible())
        {
            passwordEditor->close();
            if (editor->isVisible())
                editor->setFocus();
        }
        else
            blockMenu();
    }
}

bool MainMenu::eventFilter(QObject* target, QEvent* event)
{
    if (target == editor)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = (QKeyEvent *)event;
            if (keyEvent->key() == Qt::Key_Escape)
                editor->close();
            if (keyEvent->key() == Qt::Key_Return || keyEvent->key() == Qt::Key_Enter)
                newContent();
        }
    }
    return QWidget::eventFilter(target, event);
}

void MainMenu::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Alt)
    {
        text_color = 0;
        alt_key_pressed = false;
        table->setCurrentCell(0, 3);
        table->clearSelection();
    }
}

void MainMenu::followLink(int i)
{
    if (alt_key_pressed)
    {
        table->setCurrentCell(i, 3);
        if (table->item(i, 3) != 0 && !table->item(i, 3)->text().isEmpty())
        {
            text_color = 0;
            alt_key_pressed = false;
            QString url = table->item(i, 3)->text();
            if (!url.isEmpty())
                QDesktopServices::openUrl(QUrl(url));
        }
    }
}

void MainMenu::linkSelect(int row)
{
    if (alt_key_pressed)
    {
        table->clearSelection();
        if ((table->item(row, 0) && table->item(row, 1) && table->item(row, 2) && table->item(row, 3)) != 0)
        {
            if (table->item(row, 3)->text().isEmpty())
                text_color = 1;
            else
                text_color = 2;
            table->item(row, 0)->setSelected(true);
            table->item(row, 1)->setSelected(true);
            table->item(row, 2)->setSelected(true);
        }
    }
}

void MainMenu::openManual()
{
    passwordEditor->close();
    manual->show();
    manual->setFocus();
}
