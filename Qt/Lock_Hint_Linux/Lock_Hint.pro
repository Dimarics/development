QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

LIBS += -lX11

VERSION = 1.0.0
QMAKE_TARGET_COMPANY = Dimas Soft
QMAKE_TARGET_PRODUKT = Lock Hint
QMAKE_TARGET_DESCRIPTION = ...

RC_ICONS = icon.ico

SOURCES += \
    block.cpp \
    cryptography.cpp \
    decryption.cpp \
    editor.cpp \
    main.cpp \
    manual.cpp \
    menu.cpp \
    passedit.cpp \
    passwordline.cpp \
    style.cpp \
    table.cpp \
    window.cpp

HEADERS += \
    block.h \
    editor.h \
    manual.h \
    menu.h \
    passedit.h \
    passwordline.h \
    settings.h \
    table.h \
    window.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
