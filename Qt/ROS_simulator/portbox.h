#ifndef PORTBOX_H
#define PORTBOX_H

#include <QComboBox>
#include <QtSerialPort/QSerialPortInfo>

class PortBox : public QComboBox
{
    Q_OBJECT
public:
    explicit PortBox(QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent*);

private:
    void checkPorts();
};

#endif // PORTBOX_H
