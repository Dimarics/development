#include "portbox.h"

PortBox::PortBox(QWidget *parent) : QComboBox(parent)
{
    checkPorts();
}

void PortBox::mousePressEvent(QMouseEvent *event)
{
    checkPorts();
    QComboBox::mousePressEvent(event);
}

void PortBox::checkPorts()
{
    blockSignals(true);
    clear();
    for (QSerialPortInfo &port_info : QSerialPortInfo::availablePorts())
        addItem(port_info.portName(), QVariant::fromValue(port_info));
    blockSignals(false);
}
