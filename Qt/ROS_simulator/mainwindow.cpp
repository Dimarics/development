#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow),
    pos(1), start_session_time(QTime::currentTime().msecsSinceStartOfDay()),
    player (new QMediaPlayer), serial_port (new QSerialPort(this))
{
    ui->setupUi(this);
    qApp->installEventFilter(this);
    ui->buttonBox->close();
    ui->positions->item(0)->setSelected(true);
    ui->videoWidget->setAspectRatioMode(Qt::KeepAspectRatioByExpanding);
    connect(ui->portBox, &QComboBox::currentIndexChanged, this, &MainWindow::portChanged);
    connect(ui->quitButton, &QPushButton::clicked, qApp, &QApplication::quit);
    connect(ui->hideButton, &QPushButton::clicked, this, &QWidget::showMinimized);

    player->setLoops(QMediaPlayer::Infinite);
    player->setSource(QUrl("Animation.mp4"));
    player->setVideoOutput(ui->videoWidget);
    connect(player, &QMediaPlayer::positionChanged, this, &MainWindow::timeStamp);
    player->play();

    portChanged();

    QTimer *timer = new QTimer;
    timer->setInterval(7);
    connect(timer, &QTimer::timeout, this, &MainWindow::setTime);
    timer->start();

    QTimer *port_time = new QTimer;
    port_time->setInterval(300);
    connect(port_time, &QTimer::timeout, this, &MainWindow::readPort);
    port_time->start();
}

void MainWindow::portChanged(quint8 index)
{
    serial_port->close();
    serial_port->setPort(ui->portBox->itemData(index, Qt::UserRole).value<QSerialPortInfo>());
    qDebug() << serial_port->open(QIODeviceBase::ReadWrite);
    //qDebug() << ui->portBox->itemData(index, Qt::UserRole).value<QSerialPortInfo>().portName();
}

void MainWindow::timeStamp(qint64 position)
{
    enum Positions
    {
        Pos_1 = 14200,
        Pos_2 = Pos_1 + 9600,
        Pos_3 = Pos_2 + 8200,
        Pos_4 = Pos_3 + 10100,
        Pos_5 = Pos_4 + 10100,
        Pos_6 = Pos_5 + 8200,
        Pos_7 = Pos_6 + 9000,
        Pos_8 = Pos_7 + 10100,
        Pos_9 = Pos_8 + 10000,
        Pos_10 = Pos_9 + 9000,
        Pos_11 = Pos_10 + 9700, // 108200
        Pos_12 = Pos_11 + 8100
    };
    static qint64 mark = Pos_1;

    if (position >= mark && !(mark == Pos_1 && position > Pos_12))
    {
        QTimer::singleShot((mark == Pos_1 ? 12000 : 5000) / player->playbackRate(),
                           this, &MainWindow::selectPosition);
        switch (mark)
        {
        case Pos_1:
            mark = Pos_2;
            serial_port->write("b");
            break;
        case Pos_2:
            mark = Pos_3;
            serial_port->write("c");
            break;
        case Pos_3:
            mark = Pos_4;
            serial_port->write("d");
            break;
        case Pos_4:
            mark = Pos_5;
            serial_port->write("e");
            break;
        case Pos_5:
            mark = Pos_6;
            serial_port->write("f");
            break;
        case Pos_6:
            mark = Pos_7;
            serial_port->write("g");
            break;
        case Pos_7:
            mark = Pos_8;
            serial_port->write("h");
            break;
        case Pos_8:
            mark = Pos_9;
            serial_port->write("i");
            break;
        case Pos_9:
            mark = Pos_10;
            serial_port->write("j");
            break;
        case Pos_10:
            mark = Pos_11;
            serial_port->write("k");
            break;
        case Pos_11:
            mark = Pos_12;
            serial_port->write("l");
            break;
        case Pos_12:
            mark = Pos_1;
            serial_port->write("a");
            start_session_time = QTime::currentTime().msecsSinceStartOfDay();
            break;
        default:
            break;
        }
    }
}

void MainWindow::selectPosition()
{
    static qint8 pos = 0;
    ui->positions->item(pos)->setSelected(false);
    pos = pos < 11 ? pos + 1 : 0;
    ui->positions->item(pos)->setSelected(true);
}

void MainWindow::readPort()
{
    struct ServoData
    {
      uint16_t servo_1;
      uint16_t servo_2;
      uint16_t servo_3;
      uint16_t servo_4;
      uint16_t servo_5;
      uint16_t servo_6;
    };
    ServoData servo_data;
    serial_port->read((char*)&servo_data, sizeof(servo_data));
    ui->servo_1->setText(QString::number(servo_data.servo_1));
    ui->servo_2->setText(QString::number(servo_data.servo_2));
    ui->servo_3->setText(QString::number(servo_data.servo_3));
    ui->servo_4->setText(QString::number(servo_data.servo_4));
    ui->servo_5->setText(QString::number(servo_data.servo_5));
    ui->servo_6->setText(QString::number(servo_data.servo_6));
}

void MainWindow::setTime()
{
    static const QTime start_time = QTime::currentTime();
    QTime time = QTime::currentTime();
    ui->time->setText(time.toString("hh:mm:ss"));
    ui->ros_time->setText(QString::number(time.msecsSinceStartOfDay() - start_time.msecsSinceStartOfDay()));
    ui->session_time->setText(QString::number(time.msecsSinceStartOfDay() - start_session_time));
}

bool MainWindow::eventFilter(QObject *target, QEvent *event)
{
    if (event->type() == QEvent::MouseMove && qApp->overrideCursor()->shape() == Qt::BlankCursor)
    {
        qApp->changeOverrideCursor(QCursor(Qt::ArrowCursor));
        ui->buttonBox->show();
        QTimer::singleShot(5000, this, &MainWindow::hideButtonBox);
    }
    return QMainWindow::eventFilter(target, event);
}

void MainWindow::hideButtonBox()
{
    qApp->changeOverrideCursor(QCursor(Qt::BlankCursor));
    ui->buttonBox->close();
}
