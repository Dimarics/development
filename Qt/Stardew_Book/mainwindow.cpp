#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QScroller>
#include <QFontDatabase>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSize button_size(55 * y, 55 * y);
    QFontDatabase::addApplicationFont(":/res/resourses/ArialNova-LightItalic.ttf");
    QFontDatabase::addApplicationFont("NotoColorEmoji.ttf");
    setScroller(ui->listScrollArea);

    ui->home_button->setMinimumSize(button_size);
    ui->home_button->setMaximumSize(button_size);
    ui->home_button->setIcon(QPixmap(":/res/resourses/Home.png").scaled(button_size, Qt::KeepAspectRatio));
    ui->home_button->setIconSize(button_size);
    ui->back_button->setMinimumSize(button_size);
    ui->back_button->setMaximumSize(button_size);
    ui->back_button->setIcon(QPixmap(":/res/resourses/Back.png").scaled(button_size, Qt::KeepAspectRatio));
    ui->back_button->setIconSize(button_size);

    //----------------------------Растения-------------------------------------
    setScroller(ui->plantScrollArea);
    ui->plants_button->setFont(QFont("Noto Color Emoji"));
    ui->plant_description->setFont(QFont("Arial Nova Light", 0, QFont::Light, true));
    ui->plants_button->setMinimumHeight(ui->plants_button->minimumHeight() * y);
    connect(ui->plant_recipes, &QLabel::linkActivated, this, &MainWindow::linkTransfer);
    //------------------------------Рыба---------------------------------------
    ui->fish_button->setMinimumHeight(ui->fish_button->minimumHeight() * y);
    ui->fish_button->setFont(QFont("Noto Color Emoji"));
    //------------------------------Еда----------------------------------------
    setScroller(ui->cookingScrollArea);
    ui->cooking_button->setMinimumHeight(ui->cooking_button->minimumHeight() * y);
    ui->meal_description->setFont(QFont("Arial Nova Light", 0, QFont::Light, true));
    ui->energy_icon->setPixmap(QPixmap(":/res/resourses/Energy.png").scaled(60, 60, Qt::KeepAspectRatio));
    ui->health_icon->setPixmap(QPixmap(":/res/resourses/Health.png").scaled(60, 60, Qt::KeepAspectRatio));
    connect(ui->ingredients, &QLabel::linkActivated, this, &MainWindow::linkTransfer);
    connect(ui->meal_recipes, &QLabel::linkActivated, this, &MainWindow::linkTransfer);

    //QString adr = QString::number(quint64(ui->plant_info));
    //ui->stackedWidget->setCurrentWidget((QWidget*)adr.toLongLong());
    connect(ui->home_button, &QPushButton::clicked, this, &MainWindow::returnMainMenu);
    connect(ui->stackedWidget, &QStackedWidget::currentChanged, this, &MainWindow::pageChanged);
    connect(ui->back_button, &QPushButton::clicked, this, &MainWindow::returnBack);
    connect(ui->plants_button, &QPushButton::clicked, this, &MainWindow::plantsPageInit);
    connect(ui->fish_button, &QPushButton::clicked, this, &MainWindow::fishPageInit);
    connect(ui->cooking_button, &QPushButton::clicked, this, &MainWindow::cookingPageInit);
    returnMainMenu();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::returnMainMenu()
{
    page_history.clear();
    page_history << 0;
    ui->stackedWidget->setCurrentWidget(ui->mainMenu);
}

void MainWindow::pageChanged(const quint8 index)
{
    if (index != 0 && memory) page_history << index;
    else memory = true;
}

void MainWindow::returnBack()
{
    if (page_history.last() != 0)
    {
        memory = false;
        page_history.removeLast();
        ui->stackedWidget->setCurrentWidget(ui->stackedWidget->widget(page_history.last()));
    }
}

void MainWindow::setScroller(QScrollArea *scrollArea)
{
    QScrollerProperties scrollProperties;
    scrollProperties.setScrollMetric(QScrollerProperties::DragStartDistance, 0); // расстояние начала перетаскивания
    //scrollProperties.setScrollMetric(QScrollerProperties::DragVelocitySmoothingFactor, 1);
    scrollProperties.setScrollMetric(QScrollerProperties::OvershootDragDistanceFactor, 0.05);
    scrollProperties.setScrollMetric(QScrollerProperties::OvershootScrollDistanceFactor, 0.15);
    //scrollProperties.setScrollMetric(QScrollerProperties::DecelerationFactor, 0.3); // скорость прокрутки
    QScroller::scroller(scrollArea)->setScrollerProperties(scrollProperties);
    QScroller::grabGesture(scrollArea, QScroller::TouchGesture);
}

QString MainWindow::image(const QString &path, const quint16 size, const QString &val)
{
    return QString("<img src=\":/res/resourses/%1.png\" %2=\"%3\" style=\"vertical-align:middle;padding:50px\"/>").
            arg(path).arg(val).arg(size * y);
}

QString MainWindow::iconLabel(const QString &icon, const QString &text, const quint8 padding)
{
    return QString("<table><tr style=\"vertical-align:middle\"><td>%1</td><td style=\"vertical-align:middle; padding-left:%2\">%3</td></tr></table>").
                   arg(icon).arg(padding).arg(text);
}

QString MainWindow::link(const QString &icon, const QString &name, const QString &type)
{
    if (name == "Любая рыба")
        return name;
    else
        return QString("<a href=\"%1|%2|%3\" style=\"color:#0000E6;text-decoration:none\">%2</a>").arg(type).arg(name).arg(icon);
}

QString MainWindow::tableItem(const QString &image, const QString &text, bool table)
{
    QString picture = "</td><td style=\"padding-right:15%;padding-bottom:10%\">" + image + "</td><td style=\"padding-bottom:10%\">";
    QString table_item =  QString("<tr style=\"vertical-align:middle\"><td style=\"padding-right:15%;padding-bottom:10%\">%1</td></tr>")
                   .arg(text).replace("☐", picture);
    if (table) return "<table>" + table_item + "</table>";
    else return table_item;
}

QString MainWindow::emoji(QString text, const QString &emoji) {
    return text.replace("☐", "<span style=\"font-family:'Noto Color Emoji',\">" + emoji + "</span>");
}

void MainWindow::linkTransfer(const QString &href)
{
    QStringList data = href.split("|");
    QHash<QString, void (MainWindow::*)(const QString&, const QString&)> pageOpen;

    pageOpen["Растение"] = &MainWindow::plantInfoOpen;
    pageOpen["Еда"] = &MainWindow::cookingInfoOpen;
    pageOpen["Рыба"] = &MainWindow::fishInfoOpen;
    (this->*pageOpen[data.at(0)])(data.at(1), data.at(2));
}

void MainWindow::buttonList(const QString list[][2], const quint16 size, void (MainWindow::*page_info)())
{
    const QString byttonStyle =
            "border: 2px solid blue;"
            "background-color: rgba(255, 255, 255, 100);"
            "padding-left: 60px;"
            "border-radius: 20px;"
            "text-align: left";
    //--------------------------------------------------------
    QLayoutItem *item;
    while ((item = ui->listPageLayout->takeAt(0)) != 0)
    {
        item->widget()->deleteLater();
        delete item;
    }
    //--------------------------------------------------------
    for (quint8 i = 0; i < size / sizeof(QString[2]); i++)
    {
        ListButton *button = new ListButton;
        ui->listPageLayout->addWidget(button);
        button->setText("   " + list[i][0]);
        button->setImage(60 * y, list[i][1]);
        button->setMinimumHeight(100 * y);
        button->setStyleSheet(byttonStyle);
        connect(button, &QPushButton::clicked, this, page_info);
    }
    ui->listPageLayout->addStretch();
}

void MainWindow::setSeason(QLabel *label, const QStringList &seasons)
{
    QString str;
    for (const QString &season : seasons)
    {
        const QString icon = season == "Весна" ? "Spring" : season == "Лето" ? "Summer" :
                             season == "Осень" ? "Fall" : season == "Зима" ? "Winter" : "All_Seasons";
        str += image(icon, 36, "width") + "  " + season + "<br>";
    }
    str.chop(4);
    label->setText(str);
}