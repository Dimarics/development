#ifndef QUALITYIMAGE_H
#define QUALITYIMAGE_H

#include <QObject>
#include <QLabel>
#include <QResizeEvent>

class QualityImage : public QLabel
{
    Q_OBJECT

public:
    const qreal y = qreal(screen()->geometry().height() / 1400.0);
    QLabel *star;
    explicit QualityImage(QWidget* = 0);
    void setQuality(const QString&);
    void renderItem(const QString&);

protected:
    void resizeEvent(QResizeEvent*);
};

#endif // QUALITYIMAGE_H
