#ifndef LISTBUTTON_H
#define LISTBUTTON_H

#include <QPushButton>
#include <QMouseEvent>

class ListButton : public QPushButton
{
    Q_OBJECT

public:
    QString image_path;
    ListButton(QWidget* = 0);
    void setImage(const quint16, const QString&);

protected:
    void mousePressEvent(QMouseEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);

private:
    bool press = false;
};

#endif // LISTBUTTON_H
