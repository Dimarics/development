#ifndef PASSWORDWIDGET_H
#define PASSWORDWIDGET_H

#include <QDialog>
#include <QProcess>
namespace Ui {
class PasswordWidget;
}

class PasswordWidget : public QDialog
{
    Q_OBJECT

public:
    explicit PasswordWidget(QWidget *parent = nullptr);
    ~PasswordWidget();

private:
    int count = 0;
    bool reset = false;
    QProcess *process;
    Ui::PasswordWidget *ui;

private slots:
    void passwordVision(bool);
    void enterPassword();
    void passwordError();
    void acceptPassword();
    void hideError();

signals:
    void passwordAccepted(const QString&);
};

#endif // PASSWORDWIDGET_H
