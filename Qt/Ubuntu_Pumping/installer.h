#ifndef INSTALLER_H
#define INSTALLER_H

#include <QDialog>
#include <QProcess>

namespace Ui {
class Installer;
}

class Installer : public QDialog
{
    Q_OBJECT

public:
    explicit Installer(QWidget *parent = nullptr);
    void setCommandList(const QString&, QStringList&);
    ~Installer();

private:
    int size;
    int count = 0;
    QProcess *process;
    Ui::Installer *ui;

private slots:
    void readOutput();
    void processError();

signals:
    void installFinished();
};

#endif // INSTALLER_H
