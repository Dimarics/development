#include "editor.h"
#include "ui_editor.h"

Editor::Editor(QWidget *parent) : QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint |
                                          Qt::WindowSystemMenuHint | Qt::WindowCloseButtonHint), ui(new Ui::Editor)
{
    ui->setupUi(this);
    resize(100, 100);

    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &Editor::endEdit);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &QWidget::close);

    connect(ui->add_button, &QPushButton::clicked, this, &Editor::add_command);
    connect(ui->delete_button, &QPushButton::clicked, this, &Editor::delete_command);
    connect(ui->up_button, &QPushButton::clicked, this, &Editor::up_command);
    connect(ui->down_button, &QPushButton::clicked, this, &Editor::down_command);
}

void Editor::getData(const QStringList &data)
{
    ui->programName->clear();
    ui->descriptionText->clear();
    ui->commandList->clear();
    status.clear();
    if (!data.isEmpty())
    {
        ui->programName->setText(data.at(0));
        ui->descriptionText->document()->setPlainText(data.at(1));
        status = data.at(2);
        for (int i = 0; i < data.size() - 3; i++)
        {
            ui->commandList->addItem(data.at(i + 3));
            ui->commandList->item(i)->setFlags(ui->commandList->item(i)->flags() | Qt::ItemIsEditable);
            ui->commandList->item(i)->setSizeHint(QSize(1, 24));
        }
        ui->commandList->setCurrentRow(0);
        ui->commandList->clearSelection();
    }
    else
        status = "Не установлено";
    show();
}

void Editor::add_command()
{
    int count = ui->commandList->count();
    ui->commandList->addItem("");
    ui->commandList->item(count)->setFlags(ui->commandList->item(count)->flags() | Qt::ItemIsEditable);
    ui->commandList->item(count)->setSizeHint(QSize(1, 24));
    ui->commandList->editItem(ui->commandList->item(count));
    ui->commandList->setCurrentRow(count);
}

void Editor::delete_command()
{
    if (ui->commandList->count() != 0 && ui->commandList->currentItem()->isSelected())
        ui->commandList->takeItem(ui->commandList->currentRow());
    //ui->commandList->clearSelection();
}

void Editor::up_command()
{
    int row = ui->commandList->currentRow();
    if (ui->commandList->count() != 0 && ui->commandList->currentItem()->isSelected() && row != 0)
    {
        ui->commandList->insertItem(row - 1, ui->commandList->currentItem()->text());
        ui->commandList->item(row - 1)->setFlags(ui->commandList->item(row - 1)->flags() | Qt::ItemIsEditable);
        ui->commandList->setCurrentRow(row - 1);
        ui->commandList->takeItem(row + 1);
    }
}

void Editor::down_command()
{
    int row = ui->commandList->currentRow();
    if (ui->commandList->count() != 0 && ui->commandList->currentItem()->isSelected() && row != ui->commandList->count() - 1)
    {
        ui->commandList->insertItem(row + 2, ui->commandList->currentItem()->text());
        ui->commandList->item(row + 2)->setFlags(ui->commandList->item(row + 2)->flags() | Qt::ItemIsEditable);
        ui->commandList->setCurrentRow(row + 2);
        ui->commandList->takeItem(row);
    }
}

void Editor::endEdit()
{
    QStringList data = {ui->programName->text(), ui->descriptionText->toPlainText(), status};
    for (int i = 0; i < ui->commandList->count(); i++)
        if (!ui->commandList->item(i)->text().isEmpty())
            data << ui->commandList->item(i)->text();
    emit returnData(data);
    close();
}

Editor::~Editor()
{
    delete ui;
}
