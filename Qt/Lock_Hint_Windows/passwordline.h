#ifndef PASSWORDLINE_H
#define PASSWORDLINE_H

#include <QLineEdit>

class PasswordLine : public QLineEdit
{
    Q_OBJECT
public:
    PasswordLine(QWidget *parent = 0);

protected:
    void focusOutEvent(QFocusEvent *event);

public slots:
    void showPassword(bool);
};

#endif // PASSWORDLINE_H
