#include "passedit.h"
#include "settings.h"

PasswordEdit::PasswordEdit(QWidget *parent) : QWidget (parent)
{
    btn_close = new QPushButton(this);
    btn_close->setStyleSheet(Style::closeBtn);
    btn_close->setGeometry(622, 0, 28, 28);

    QLabel *hint = new QLabel(this);
    hint->setGeometry(175, 40, 300, 40);
    hint->setStyleSheet(Style::editPasswordHint);
    hint->setText("Введите новый пароль:");
    hint->setAlignment(Qt::AlignmentFlag::AlignCenter);

    passwordLine = new PasswordLine(this);
    passwordLine->setMaxLength(26);
    passwordLine->setStyleSheet(Style::editPasswordString);
    passwordLine->setGeometry(42, 110, 520, 48);

    QPushButton *btn_vision = new QPushButton(this);
    btn_vision->setGeometry(562, 110, 48, 48);
    btn_vision->setStyleSheet(Style::visionEditPassword);
    btn_vision->setCheckable(true);
    btn_vision->setShortcut(tr("Alt+S"));
    btn_vision->setCursor(Qt::PointingHandCursor);
    connect(btn_vision, &QPushButton::toggled, passwordLine, &PasswordLine::showPassword);

    btn_ok = new QPushButton(this);
    btn_ok->setStyleSheet(Style::okBtn);
    btn_ok->setText("ок");
    btn_ok->setGeometry(288, 185, 74, 32);
}

void PasswordEdit::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QColor(120, 120, 130, 255)); //контур виджета
    painter.setBrush(QColor(60, 68, 84, 255)); //фон виджета
    painter.drawRect(QRect(0, 0, width() - 1, height() - 1));
}
