#ifndef MENU_H
#define MENU_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QMenu>
#include <QPainter>
#include <QKeyEvent>
#include <QDesktopServices>
#include <QUrl>
#include <QGraphicsDropShadowEffect>

#include "table.h"
#include "passedit.h"
#include "editor.h"
#include "manual.h"

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    QLineEdit *search;
    Table *table;
    EditWidget *editor;
    PasswordEdit *passwordEditor;
    MainMenu(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

private:
    QLabel *layout;
    QPushButton *searchClearButton;
    Manual *manual;
    bool del;
    bool control_key_pressed = false;
    bool alt_key_pressed = false;
    int row_num;
    void fillTable();
    void writeList();
    void clearRow(int);
    bool eventFilter(QObject* target, QEvent* event);

public slots:
    void switchLayout(QString);
    void deleteContent();

private slots:
    void blockMenu();
    void replaceContent(int, int);
    void addContent();
    void newContent();
    void followLink(int);
    void newPassword();
    void editingCompleted();
    void closePasswordEditor();
    void tableSearch(QString);
    void searchClear();
    void linkSelect(int);
    void openManual();

signals:
    void block(bool);
};

#endif // MENU_H
