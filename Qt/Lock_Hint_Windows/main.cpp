#include <QApplication>
#include <QPalette>
#include <QIcon>
#include "window.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QPalette palette;
    Window window;

    window.setWindowIcon(QPixmap(":/res/icon.png"));
    palette.setColor(QPalette::Window, QColor(60, 68, 83));
    //window.setPalette(palette);

    window.show();

    return app.exec();
}
