#ifndef SETTINGS_H
#define SETTINGS_H

#include <QStringList>
extern QStringList database;
void cryptography();

class Style
{
public:
    static const char *const closeBtn;
    static const char *const foldBtn;
    static const char *const stringPassword;
    static const char *const blockHint;
    static const char *const passwordError;
    static const char *const visionBlockPassword;
    static const char *const layoutFirst;
    static const char *const panelBtn;
    static const char *const contextMenu;
    static const char *const searchBar;
    static const char *const clearTextBtn;
    static const char *const layoutSecond;
    static const char *const table;
    static const char *const followSelect;
    static const char *const tableHeader;
    static const char *const tableScrollBar;
    static const char *const okBtn;
    static const char *const editLine;
    static const char *const editPasswordHint;
    static const char *const editPasswordString;
    static const char *const visionEditPassword;
    static const char *const manual;
    static const char *const backBtn;
};

#endif // SETTINGS_H
