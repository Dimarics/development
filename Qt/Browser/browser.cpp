#include "browser.h"
#include <QTimer>

Browser::Browser()
{
    connect(this, &QWebEngineView::loadFinished, this, &Browser::checkLoad);
    connect(this, &QWebEngineView::renderProcessTerminated, this, &QWebEngineView::reload);
    load(QUrl("http://192.168.0.4:5000"));

    QTimer *timer = new QTimer(this);
    timer->setInterval(60000);
    connect(timer, &QTimer::timeout, this, &QWebEngineView::reload);
    timer->start();
}

void Browser::checkLoad(bool success) {
    if (!success) reload();
}
