#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->settings->close();
    browser = new Browser;
    ui->verticalLayout->addWidget(browser);
    connect(ui->zoomSlider, &QAbstractSlider::valueChanged, this, &MainWindow::setZoom);
    connect(ui->quitButton, &QPushButton::clicked, qApp, &QCoreApplication::quit);
    qApp->installEventFilter(this);

    QFile file("zoom.txt");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString zoom = file.readLine();
        browser->setZoomFactor(zoom.toFloat());
        ui->zoom->setText(zoom);
    }
}

void MainWindow::setZoom(int value)
{
    qreal zoom = qreal(value) / 10;
    browser->setZoomFactor(zoom);
    ui->zoom->setText(QString::number(zoom, 'f', 1));

    QFile file("zoom.txt");
    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream write(&file);
        write << QString::number(zoom, 'f', 1);
    }
}

bool MainWindow::eventFilter(QObject *target, QEvent *event)
{
    if (event->type() == QEvent::MouseMove && qApp->overrideCursor()->shape() == Qt::BlankCursor)
    {
        qApp->changeOverrideCursor(QCursor(Qt::ArrowCursor));
        ui->settings->show();
        QTimer::singleShot(5000, this, &MainWindow::hideSettings);
    }
    return QMainWindow::eventFilter(target, event);
}

void MainWindow::hideSettings()
{
    qApp->changeOverrideCursor(QCursor(Qt::BlankCursor));
    ui->settings->close();
}
