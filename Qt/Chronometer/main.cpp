#include <QApplication>
#include <QtAndroidExtras/QtAndroid>

#include "mainwindow.h"
//#include <QDebug>

void checkPermissions()
{
    if (QtAndroid::androidSdkVersion() > 22)
    {
        QStringList permissions {"android.permission.READ_EXTERNAL_STORAGE",
                                 "android.permission.WRITE_EXTERNAL_STORAGE"};
        for (QString &permission : permissions)
        {
            if (QtAndroid::checkPermission(permission) != QtAndroid::PermissionResult::Granted)
                QtAndroid::requestPermissionsSync(permissions);
        }
    }
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Chronometer timer;
    checkPermissions();
    timer.show();
    return app.exec();
}
