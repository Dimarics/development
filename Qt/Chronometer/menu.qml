import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5

Item {
    Button {
        anchors.fill: parent
        background: Rectangle {
            color: Qt.rgba(0, 0, 0, 0.3)
        }
        onClicked: qmlWidget.hideQmlWidget()
    }

    Button {
        id: btnSettings
        width: 362
        height: 132
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 40
        anchors.rightMargin: 40

        background: Rectangle {
            id: btnRect
            radius: 35
        }
        contentItem: Text {
            id: label
            text: qsTr("Настройки")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            leftPadding: 40
            font.pointSize: 17
        }
    }
    Connections {
        target: qmlWidget
        onMenuStyle: {
            styleName === "Dark" ? label.color = "white" : label.color = "black"
            styleName === "Dark" ? btnRect.color = "#232323" : btnRect.color = "white"
        }
    }
}
