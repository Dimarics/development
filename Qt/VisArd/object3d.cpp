#include "object3d.h"
#include <QFile>

Object3D::Object3D() : m_color(Qt::gray), m_indexBuffer(QOpenGLBuffer::IndexBuffer) {}

Object3D::Object3D(const QString &path, const QColor &color, Object3D *parent) :
    m_color(color),
    m_indexBuffer(QOpenGLBuffer::IndexBuffer),
    m_parentObject(parent)
{
    loadStl(path);
}

Object3D::~Object3D()
{
    m_arrayBuffer.destroy();
    m_indexBuffer.destroy();
    //delete m_texture;
}

void Object3D::loadMesh(const QString &path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    QVector<QVector3D> coords;
    //QVector<QVector2D> tex_coords;
    QVector<QVector3D> normals;
    QVector<GLuint> indexes;

    while (!file.atEnd())
    {
        const QByteArrayList data = file.readLine().split(' ');
        if (data.at(0) == "v")
        {
            coords.append(QVector3D(data.at(3).toFloat(), data.at(1).toFloat(), data.at(2).toFloat()));
        }
        /*else if (data.at(0) == "vt")
                tex_coords.append(QVector2D(data.at(1).toFloat(), data.at(2).toFloat()));*/
        else if (data.at(0) == "vn")
        {
            normals.append(QVector3D(data.at(3).toFloat(), data.at(1).toFloat(), data.at(2).toFloat()));
        }
        else if (data.at(0) == "f")
        {
            for (quint8 i = 1; i <= 3; ++i)
            {
                /*float min_x = coords.at(0).x();
                float max_x = coords.at(0).x();
                float min_y = coords.at(0).y();
                float max_y = coords.at(0).y();*/
                float min_z = coords.at(0).z();

                for (const QVector3D &extreme : coords)
                {
                    /*if (extreme.x() < min_x) min_x = extreme.x();
                    if (extreme.x() > max_x) max_x = extreme.x();
                    if (extreme.y() < min_y) min_y = extreme.y();
                    if (extreme.y() > max_y) max_y = extreme.y();*/
                    if (extreme.z() < min_z) min_z = extreme.z();
                }
                for (QVector3D &point : coords) point.setZ(point.z() - min_z);

                QByteArrayList vertex = data.at(i).split('/');
                vertices.append(Vertex(coords.at(vertex.at(0).toLong() - 1), normals.at(vertex.at(2).toLong() - 1)));
                indexes.append(indexes.size());
            }
        }
    }
    file.close();

    m_arrayBuffer.create();
    m_arrayBuffer.bind();
    m_arrayBuffer.allocate(vertices.constData(), vertices.size() * sizeof(Vertex));
    m_arrayBuffer.release();

    m_indexBuffer.create();
    m_indexBuffer.bind();
    m_indexBuffer.allocate(indexes.constData(), vertices.size() * sizeof(GLuint));
    m_indexBuffer.release();
}

void Object3D::loadStl(const QString &)
{
    QFile file("model.stl");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    QVector<GLuint> indexes;
    QDataStream input(&file);

    input.setByteOrder(QDataStream::LittleEndian);
    input.setFloatingPointPrecision(QDataStream::SinglePrecision);
    quint32 vertex_number;
    file.seek(80);
    input >> vertex_number;

    for (quint32 count = 0; count < 2; ++count)
    {
        quint16 control_bytes;
        qreal nx, ny, nz, x1, y1, z1, x2, y2, z2, x3, y3, z3;
        //input >> nx >> ny >> nz >> x1 >> y1 >> z1 >> x2 >> y2 >> z2 >> x3 >> y3 >> z3 >> control_bytes;
        file.seek(84 + count * 50 + 0 + 0);
        input >> nx;
        file.seek(84 + count * 50 + 0 + 4);
        input >> ny;
        file.seek(84 + count * 50 + 0 + 8);
        input >> nz;
        file.seek(84 + count * 50 + 12 + 0);
        input >> x1;
        file.seek(84 + count * 50 + 12 + 4);
        input >> y1;
        file.seek(84 + count * 50 + 12 + 8);
        input >> z1;
        file.seek(84 + count * 50 + 24 + 0);
        input >> x2;
        file.seek(84 + count * 50 + 24 + 4);
        input >> y2;
        file.seek(84 + count * 50 + 24 + 8);
        input >> z2;
        file.seek(84 + count * 50 + 36 + 0);
        input >> x3;
        file.seek(84 + count * 50 + 36 + 4);
        input >> y3;
        file.seek(84 + count * 50 + 36 + 8);
        input >> z3;
        file.seek(84 + count * 50 + 48);
        input >> control_bytes;
        qDebug() << x1 << y1 << z1;
        vertices.append(Vertex(QVector3D(x1, y1, z1), QVector3D(nx, ny, nz)));
        vertices.append(Vertex(QVector3D(x2, y2, z2), QVector3D(nx, ny, nz)));
        vertices.append(Vertex(QVector3D(x3, y3, z3), QVector3D(nx, ny, nz)));
        indexes.append(indexes.size());
        indexes.append(indexes.size());
        indexes.append(indexes.size());
    }
    file.close();

    m_arrayBuffer.create();
    m_arrayBuffer.bind();
    m_arrayBuffer.allocate(vertices.constData(), vertices.size() * sizeof(Vertex));
    m_arrayBuffer.release();

    m_indexBuffer.create();
    m_indexBuffer.bind();
    m_indexBuffer.allocate(indexes.constData(), vertices.size() * sizeof(GLuint));
    m_indexBuffer.release();
}

void Object3D::draw(QOpenGLShaderProgram *program, QOpenGLFunctions *functions)
{
    if (!m_arrayBuffer.isCreated() || !m_indexBuffer.isCreated()) return;
    m_arrayBuffer.bind();
    m_indexBuffer.bind();
    //texture->bind();

    //program.setUniformValue("texture", 0);
    QMatrix4x4 modelMatrix;
    modelMatrix.translate(m_move + m_rotateCenter);
    modelMatrix.rotate(m_rotate);
    modelMatrix.translate(-m_rotateCenter);
    if (parent() != nullptr) modelMatrix = parent()->transform() * modelMatrix;
    m_transform = modelMatrix;

    program->setUniformValue("u_color", m_color);
    program->setUniformValue("u_modelMatrix", modelMatrix);

    int offset = 0;
    program->setAttributeBuffer("a_position", GL_FLOAT, offset, 3, sizeof(Vertex));

    offset += sizeof(QVector3D);
    program->setAttributeBuffer("a_normal", GL_FLOAT, offset, 3, sizeof(Vertex));

    functions->glDrawArrays(GL_TRIANGLES, 0, m_indexBuffer.size());

    m_arrayBuffer.release();
    m_indexBuffer.release();
    //m_texture.release();
}
/*void Mesh::initTexture()
{
    m_texture = new QOpenGLTexture(QImage(":/cube.png").mirrored());
    m_texture->setMinificationFilter(QOpenGLTexture::Nearest);
    m_texture->setMagnificationFilter(QOpenGLTexture::Linear);
    m_texture->setWrapMode(QOpenGLTexture::Repeat);
}*/

void Object3D::setColor(const QColor &color) {
    m_color = color;
}

void Object3D::move(const float x, const float y, const float z) {
    m_move += QVector3D(x, y, z);
}

void Object3D::rotate(const QQuaternion &rotate) {
    m_rotate = rotate * m_rotate;
}

void Object3D::setRotationCenter(const float x, const float y, const float z) {
    m_rotateCenter = QVector3D(x, y, z);
}

QMatrix4x4 Object3D::transform() const {
    return m_transform;
}

Object3D* Object3D::parent() const {
    return m_parentObject;
}
