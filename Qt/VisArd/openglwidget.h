#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include "camera.h"
#include "object3d.h"
#include "grid.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QQuaternion>

class GeometryEngine;

class OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    OpenGLWidget(QWidget* = nullptr);
    ~OpenGLWidget();

protected:
    void initShaders();

    void initializeGL() override;
    void resizeGL(int, int) override;
    void paintGL() override;

    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void wheelEvent(QWheelEvent*) override;

private:
    Grid *grid;
    Camera *camera;
    QList<Object3D*> objects;
    QOpenGLShaderProgram m_program;
    QMatrix4x4 m_projectionMatrix;
    QVector2D m_mousePosition;
};

#endif // OPENGLWIDGET_H
