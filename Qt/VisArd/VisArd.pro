QT += core gui widgets opengl openglwidgets

CONFIG += qt warn_off release

#TEMPLATE = app

SOURCES += main.cpp \
    camera.cpp \
    grid.cpp \
    mainwindow.cpp \
    object3d.cpp \
    openglwidget.cpp

HEADERS += \
    camera.h \
    grid.h \
    mainwindow.h \
    object3d.h \
    openglwidget.h

RESOURCES += \
    resources.qrc

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#win32-g* { LIBS += -lopengl32 }

FORMS += \
    mainwindow.ui
