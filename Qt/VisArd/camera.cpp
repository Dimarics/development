#include "camera.h"

Camera::Camera() : m_scale(1.0) {}

void Camera::view(QOpenGLShaderProgram *program)
{
    QMatrix4x4 viewMatrix;
    viewMatrix.translate(m_move);
    viewMatrix.rotate(m_rotate);
    viewMatrix.scale(m_scale);
    viewMatrix *= m_translate;
    program->setUniformValue("u_viewMatrix", viewMatrix);
}

void Camera::move(const float x, const float y, const float z)
{
    m_move += QVector3D(x, y, z);
}

void Camera::translate(const float x, const float y, const float z)
{
    QMatrix4x4 translate;
    translate.translate(x, y, z);
    m_translate = translate.inverted();
}

void Camera::relativeRotate(const QQuaternion &rotate)
{
    m_rotate = rotate * m_rotate;
}

void Camera::globalRotate(const QQuaternion &rotate)
{
    m_rotate *= rotate;
}

void Camera::scale(const float scale)
{
    m_scale *= scale;
}
