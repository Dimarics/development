//uniform sampler2D u_texture;
uniform highp bool u_lighting;
uniform highp float u_lightPower;
uniform highp vec4 u_color;
varying highp vec4 v_position;
varying highp vec3 v_normal;
//varying highp vec2 v_texcoord;

void main()
{
    //vec4 diffMatColor = texture2D(u_texture, v_texcoord);
    vec4 resultColor = u_color;
    if (u_lighting)
    {
        vec3 lightVector = normalize(v_position.xyz);
        resultColor *= u_lightPower * max(0.0, dot(v_normal, -lightVector));// / (1.0 + 0.25 * len * len);
    }

    gl_FragColor = resultColor;
}
