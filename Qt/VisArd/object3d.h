#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <QColor>
//#include <QOpenGLTexture>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QQuaternion>

class Object3D
{
public:
    Object3D();
    Object3D(const QString&, const QColor&, Object3D* = nullptr);
    ~Object3D();

    void loadMesh(const QString&);
    void loadStl(const QString&);
    void draw(QOpenGLShaderProgram*, QOpenGLFunctions*);
    void setColor(const QColor&);
    void move(const float, const float, const float);
    void rotate(const QQuaternion&);
    void setRotationCenter(const float, const float, const float);
    //void setTransform(const QMatrix4x4);
    QMatrix4x4 transform() const;
    Object3D* parent() const;

private:
    struct Vertex
    {
        Vertex(const QVector3D &p, const QVector3D &n) : position(p), normal(n) {}
        QVector3D position;
        QVector3D normal;
    };
    QVector<Vertex> vertices;
    QColor m_color;
    QOpenGLBuffer m_arrayBuffer;
    QOpenGLBuffer m_indexBuffer;
    //QOpenGLTexture *m_texture = nullptr;
    //void initTexture()
    QVector3D m_move;
    QQuaternion m_rotate;
    QVector3D m_rotateCenter;
    QMatrix4x4 m_transform;
    Object3D* m_parentObject;
};

#endif // OBJECT3D_H
