#ifndef GITPUSHER_H
#define GITPUSHER_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class GitPusher; }
QT_END_NAMESPACE

class GitPusher : public QMainWindow
{
    Q_OBJECT

public:
    GitPusher(QWidget *parent = nullptr);
    ~GitPusher();

private:
    Ui::GitPusher *ui;
};
#endif // GITPUSHER_H
